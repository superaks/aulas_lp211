<?php

class Table {

  private $data;
  private $label;

  function __construct(array $data, array $label) {
    $this -> data = $data;
    $this -> label = $label;
  }

  private function header() {
    $html = '<thead>
      <tr>';

    foreach($this -> label as $label) {
      $html .= '<th>' . $label . '</th>';
    }
    
    $html .=  '</tr>
    </thead>';
    return $html;
  }

  private function body() {
    $html = '<tbody>'.
              $this -> rows(). 
          '</tbody>';
    return $html;
  }

  private function rows() {
    $html = '';

    foreach($this -> data as $rows) {
      $html .= '<tr id="conta_' . $rows['id'] . '">';
      unset($rows['id']);
        foreach($rows as $cols) {
          $html .= '<td>' . $cols .'</td>';
        }
      $html .= '</tr>';
    }


    return $html;
  }

  public function getHtml() {
    $html = '<table class="table"> '.
      $this -> header().
      $this -> body().
    '</table>';
    return $html;
  }
}
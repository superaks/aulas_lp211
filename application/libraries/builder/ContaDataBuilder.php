<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ContaDataBuilder extends CI_Object {

  protected $contas = [
    [
      'parceiro' => 'Magalu', 
      'descricao' => 'Notebook', 
      'valor' => '1500', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'pagar'
    ],
    [
      'parceiro' => 'Prefeitura de São Paulo', 
      'descricao' => 'Salário', 
      'valor' => '3542.18', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'receber'
    ],
    [
      'parceiro' => 'Aluguel', 
      'descricao' => 'Casa Alugada', 
      'valor' => '680', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'receber'
    ],
    [
      'parceiro' => 'Bandeirante', 
      'descricao' => 'Energia Elétrica', 
      'valor' => '97.25', 
      'mes' => 1, 
      'ano' => 2021, 
      'tipo' => 'pagar'
    ],
    [
      'parceiro' => 'Bandeirante', 
      'descricao' => 'Energia Elétrica', 
      'valor' => '97.25', 
      'mes' => 2, 
      'ano' => 2021, 
      'tipo' => 'pagar'
    ]
  ];

  public function start() {
    $this -> load -> library('Conta');

    foreach ($this -> contas as $conta) {
      $res = $this -> conta -> cria($conta);
    }

  }

  public function clear() {

    $this -> db -> truncate('conta');

  }
  
}

<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  require_once APPPATH . 'libraries/component/Table.php';

  class ContasModel extends CI_Model {

    public function __construct() {
      $this -> load -> library('conta', '', 'bill');
    }

    public function cria($tipo) {
      if(sizeof($_POST) == 0) return ;

      list($ano, $mes) = explode('-', $_POST['month']);
      $_POST['ano'] = $ano;
      $_POST['mes'] = $mes;

      $data = $this -> input -> post();
      $this -> validate();

      if($this -> form_validation -> run()) {
        if($data['id']) {
          $this -> bill -> edita($data);
        } else {
          $this -> bill -> cria($data);
        }
      }


    }

    private function validate() {
      $this -> form_validation -> set_rules('parceiro', 'Parceiro Comercial', 'required|min_length[5]|max_length[100]');
      $this -> form_validation -> set_rules('descricao', 'Descrição da Conta', 'required|min_length[5]|max_length[100]');
      $this -> form_validation -> set_rules('valor', 'Preço a ser Pago', 'required|greater_than[0]');
      $this -> form_validation -> set_rules('mes', 'Mês de pagamento', 'required|greater_than[0]|less_than[13]');
      $this -> form_validation -> set_rules('ano', 'Ano de pagamento', 'required|greater_than[2019]|less_than[2030]');
    }

    public function lista($tipo, $mes, $ano) {
      $data = [];
      $v = $this -> bill -> lista($tipo, $mes, $ano);

      foreach($v as $cols) {
        $aux['parceiro'] = $cols['parceiro'];
        $aux['descricao'] = $cols['descricao'];
        $aux['valor'] = $cols['valor'];
        $aux['btn'] = $this -> getActionButton($cols['liquidada']);
        $aux['id'] = $cols['id'];
        $data[] = $aux;
      }

      $label = ['Parceiro', 'Descrição', 'Valor', 'Ação'];

      $table = new Table($data, $label);
      return $table -> getHtml();
    }

    private function getActionButton($liquidada) {
      $html = '<a><i title="Pagar" class="fas ' . (($liquidada % 2 == 1) ? 'text-success' : 'text-dark') . ' me-2 pay_btn fa-check"></i></a>';
      $html .= '<a><i title="Editar" class="fas fa-pen me-2 text-primary edit_btn"></i></a>';
      $html .= '<a><i title="Deletar" class="fas fa-trash text-danger me-2 delete_btn" data-mdb-toggle="modal" data-mdb-target="#exampleModal"></i></a>';
      return $html;
    }

    public function delete_conta() {
      $data = $this -> input -> post();
      $this -> bill -> delete($data);
    }

    public function status_conta() {
      $data = $this -> input -> post();
      $this -> bill -> status($data);
    }

  }


?>
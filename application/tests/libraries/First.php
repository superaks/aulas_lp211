<?php

  class First extends TestCase {

    public function setUp(): void {
      $this -> resetInstance();
    }

    public function xtestHello() {
      $this -> CI -> load -> library('Conta');
      $res = $this -> CI -> conta -> soma(4, 3);
      $this -> assertEquals(7, $res);
    }


    public function xtestContaDeveInserirRegistroCorretamente() {

      // cenário
      $this -> CI -> load -> library('builder/ContaDataBuilder', null, 'builder');
      $this -> CI -> builder -> clear();
      $this -> CI -> builder -> start();

      // ação
      $this -> CI -> load -> library('Conta');
      $res = $this -> CI -> conta -> lista('pagar', 1, 2021);


      $this -> assertEquals(2, sizeof($res));

      $this -> assertEquals('Magalu', $res[0]['parceiro']);
      $this -> assertEquals(2021, $res[0]['ano']);
      $this -> assertEquals(1, $res[0]['mes']);

      $this -> assertEquals('Bandeirante', $res[1]['parceiro']);
      $this -> assertEquals(2021, $res[1]['ano']);
      $this -> assertEquals(1, $res[1]['mes']);
      $this -> assertEquals('97.25', $res[1]['valor']);
    }

    function xtestBuilder() {
      $this -> CI -> load -> library('builder/ContaDataBuilder', null, 'builder');
      $this -> CI -> builder -> clear();
      $this -> CI -> builder -> start();
      $this -> assertEquals(1, 1);


    }

    function xtestContaDeveInformarTotalDeContasEAPagarEReceber() {
      $this -> CI -> load -> library('builder/ContaDataBuilder', null, 'builder');
      $this -> CI -> builder -> clear();
      $this -> CI -> builder -> start();

      $this -> CI -> load -> library('Conta');
      $res = $this -> CI -> conta -> total('pagar', 1, 2021);

      $this -> assertEquals(1597.25, $res);
    }

    function testContaDeveCalcularSaldoMensal() {
      $this -> CI -> load -> library('builder/ContaDataBuilder', null, 'builder');
      $this -> CI -> builder -> clear();
      $this -> CI -> builder -> start();

      // ação
      $this -> CI -> load -> library('Conta');
      $res = $this -> CI -> conta -> saldo(1, 2021);


      $this -> assertEquals(2624.93, $res);

    }

  }
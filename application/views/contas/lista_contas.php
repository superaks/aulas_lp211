<div class="container">

  <div class="row mt-5">
    <div class="col-md-2">
      <a
        class="btn btn-primary"
        data-mdb-toggle="collapse"
        href="#collapseForm"
        role="button"
        aria-expanded="false"
        aria-controls="collapseForm"
      >
        Nova Conta
      </a>
    </div>
    <div class="col-md-2 offset-md-7 mt-3">
      <input type="month" id="month" name="month" value="<?= set_value('month') ?>">
    </div>
  </div>
  <?php echo form_error('mes', '<div class="alert mt-4 alert-danger">', '</div>'); ?>
  <?php echo form_error('ano', '<div class="alert mt-4 alert-danger">', '</div>'); ?>
  <?php echo form_error('descricao', '<div class="alert mt-4 alert-danger">', '</div>'); ?>
  <?php echo form_error('valor', '<div class="alert mt-4 alert-danger">', '</div>'); ?>
  <?php echo form_error('parceiro', '<div class="alert mt-4 alert-danger">', '</div>'); ?>
  <div class="collapse mt-3" id="collapseForm">
    <div class="row">
      <div class="col-md-6 mx-auto border mt-5 pt-5 pb-3">
        <form method="POST" id="contas-form">
          <input class="form-control" value="<?= set_value('parceiro') ?>" name="parceiro" placeholder="Devedor / Credor" type="text"><br>
          <input class="form-control" value="<?= set_value('descricao') ?>" name="descricao" placeholder="Descrição" type="text"><br>
          <input class="form-control" value="<?= set_value('valor') ?>" name="valor" placeholder="Valor" type="number"><br>
          <input type="hidden" name="tipo" value="<?= $tipo ?>">
          <input type="hidden" id="conta_id" name="id" >


          <div class="text-center text-md-left mt-4">
            <a class="btn btn-primary" onclick="document.querySelector('#contas-form').submit()">Enviar</a>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="row mt-5">
    <div class="col">
      <?= $lista ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div
  class="modal fade"
  id="exampleModal"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button
          type="button"
          class="btn-close"
          data-mdb-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        Deseja deletar sua conta?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
          Fechar
        </button>
        <button type="button" class="btn btn-primary" data-mdb-dismiss="modal" id="confirmBtn">Deletar</button>
      </div>
    </div>
  </div>
</div>

<script>
  let row_id = 0;

  $(document).ready(function() {
    $('#month').on('change', loadMonth)
    $('.delete_btn').click(openModal)
    $('#confirmBtn').click(deleteRow)
    $('.edit_btn').click(exibeForm)
    $('.pay_btn').click(liquidaConta)
  })

  function openModal() {
    row_id = getId(this)
    $('#exampleModal').modal()
  }

  function exibeForm() {
    const id = getId(this)
    const td = $(this).closest('tr').children()
    $('#collapseForm').collapse('show')
    $('input[name=parceiro]').val(td.eq(0).text())
    $('input[name=descricao]').val(td.eq(1).text())
    $('input[name=valor]').val(td.eq(2).text())
    $('input[name=mes]').val(td.eq(3).text())
    $('input[name=ano]').val(td.eq(4).text())
    $('#conta_id').val(id)
  }


  function deleteRow() {
    const id = row_id

    $.post(api('contas', 'delete_conta'), {id})
      .done(console.log)
      .always(() => window.location = window.location.href)
  }

  async function liquidaConta() {
    const id = getId(this)
    try {
      const result = await $.post(api('contas', 'status_conta'), {id})
      
      $(this).toggleClass('text-dark text-success')
    } catch(e) {
      console.log(e)
    }
  }

  function loadMonth() {
    const values = this.value.split('-')
    const ano = values[0]
    const mes = values[1]
    
    const url = window.location.href.split('/').slice(0, 7).join('/') + `/${mes}/${ano}`
    window.location.href = url
  }

  function getId(element) {
    const id = $(element).closest('tr').attr('id').match(/\d+/)[0]
    return id
  }
</script>
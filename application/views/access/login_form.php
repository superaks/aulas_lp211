<div class="container">
  <div class="d-flex flex-column justify-content-center align-items-center" style="height: 100vh;">
    
    <h3 class="mb-5">Controle Financeiro Pessoal</h3>
    <form class="text-center border border-light p-2" method="post">
      <h3 style="text-align: center;">Entrar</h3>
      <div class="form-outline mb-4">
        <input type="email" name="email" id="form2Example1" class="form-control" />
        <label class="form-label" for="form2Example1">Email address</label>
      </div>
      <div class="form-outline mb-4">
        <input type="password" name="password" id="form2Example2" class="form-control" />
        <label class="form-label" for="form2Example2">Password</label>
      </div>

      <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>
      <p class="text-danger"><?= $error ? 'Dados de acesso incorreto' : '' ?></p>
      </div>
    </form>

  </div>
</div>